from os import environ
from sys import exit
import logging
from app import app


if __name__ == "__main__":
    app.run()
