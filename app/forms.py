from wtforms import Form 
from wtforms import StringField, TextField, SelectField

class CommentForm(Form):
    keys_words = TextField('keys_words')
    ord_number = TextField('ord_number')
    ord_type = SelectField('type')
