import os
import io
import logging
from importlib import import_module

from flask import Flask, flash, render_template, url_for, request, redirect, session, send_file
from flask_tryton import Tryton
from flask_babel import Babel
from flask_wtf import FlaskForm
from datetime import datetime
from flask import request
import app.forms

from functools import wraps

from wtforms import StringField
from wtforms.validators import DataRequired
from wtforms.fields import PasswordField

from config import config_dict
from decouple import config

app=Flask(__name__)

links = []

DEBUG = config('DEBUG', default=True, cast=bool)
get_config_mode = 'Debug' if DEBUG else 'Production'
app_config = config_dict[get_config_mode.capitalize()]
app.config.from_object(app_config)

SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
app.config['TRYTON_DATABASE'] = os.environ.get('digesto','digesto')
app.config['TRYTON_CONFIG'] = 'trytond.conf'
app.config['TRYTON_USER'] = 1

tryton = Tryton(app, configure_jinja=True)

Digesto = tryton.pool.get('digesto.digesto')

@app.route("/", methods=['POST', 'GET'])
@tryton.transaction()
def search():
    keys_words = ""
    ord_number = ""
    ord_type = ""
    form = forms.CommentForm(request.form)
    if request.method == 'POST':
        keys_words = form.keys_words.data
        ord_number = form.ord_number.data
        if form.ord_type.data is not None:
            ord_type = form.ord_type.data
        links.append(keys_words+ord_number+ord_type)
        #links.append("https://www.google.com/")
    #document_type =\
        #'decreto' if ord_type == 'Decreto' else\
        #'orden' if ord_type == 'Orden' else\
        #'reglamentacion' if ord_type == 'Reglamentación' else ''
    digestos = Digesto.search(
        ['OR',
        ('keywords','ilike','\%'+keys_words+'\%'),
        ('name','=',ord_number),
        #('document_type','=',document_type)
        ])
    return render_template('main.html', links=links, digestos=digestos)

@app.route('/download/<id>')
@tryton.transaction()
def render_report(id):
    digesto = Digesto.search([('id','=',id)])
    ext = digesto[0].document_name.split('.')[-1]
    content = digesto[0].document
    name = ''.join(['(',digesto[0].name,')',digesto[0].document_name.split('.')[0]])
    return send_file(
        io.BytesIO(content),
        as_attachment=True,
        attachment_filename='%s.%s' % (name, ext))
